/**
 * @file
 * JavaScript behavior for updating Drupal.progressBar with upload progress information.
 */

Drupal.behaviors.upload_progress = {
  attach: function (context, settings) {

    jQuery("input[type=submit]", ".file-widget, .image-widget").each(function(el) {
      var ajax = Drupal.ajax[this.id];
      ajax.progress.url = undefined;
      ajax.options.uploadProgress = function(event, partial, total, percentage) {
        if (ajax.progress.object) {
          ajax.progress.object.setProgress(percentage, Drupal.t("Uploading... (current of total)", {
            current: Drupal.upload_progress.bytes2string(partial),
            total: Drupal.upload_progress.bytes2string(total)
          }));
        }
      }
    });

  }
}


/**
 * Get human-readable string (e.g. 1.25 MB) from amount of bytes.
 */
Drupal.upload_progress = Drupal.upload_progress || {
  bytes2string: function (bytes) {
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1000)));
    return (bytes / Math.pow(1000, i)).toPrecision(3) + " " + ["B", "KB", "MB", "GB"][i];
  }
}
