INTRODUCTION
------------
The upload_progress module for Drupal 7 uses client-side JavaScript to display
the progress of an upload in progress.
It does not require the uploadprogress PECL module or APC and thus works great
on configurations without Apache/mod_php, i.e. FastCGI/fcgi on Apache or Nginx
or Lighttpd.
It uses the ProgressEvent fired by XMLHttpRequest in order to update the
upload progress bar.

REQUIREMENTS
------------
* [Libraries](https://www.drupal.org/project/libraries) is required for
installing and using upload_progress.

INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information
* Download a recent version (3.51 as of May 2015) of the
[JQuery Form Plugin](http://malsup.com/jquery/form/#download) and place it in
**sites/all/libraries/jquery_form**.

upload_progress will look for the JQuery Form library in
**sites/all/libraries/jquery_form/jquery.form.min.js** or
**sites/all/libraries/jquery_form/jquery.form.js**.

CONFIGURATION
-------------
This module has no menu or modifiable settings. There is no configuration.
When enabled, it replaces the jquery.form library shipped by Drupal 7 with
the one installed in the libraries folder.

MAINTAINERS
-----------
Current maintainers:
* wmnnd - https://www.drupal.org/user/172476

This project was sponsored by:
* vanbittern - https://vanbittern.com
